#ifndef __symbol_c__
#define __symbol_c__

#include <string.h>
#include "hoc.h"
#include "y.tab.h"

static Symbol * symlist = 0;

/**lookup****************************************
 *  Find a symbol matching supplied string. 	*
 * 	@param s cstring, name of matching symbol. 	*
 *  @return sp pointer to the matching symbol.  *
 *  @return 0 if no match is found.				*
 ************************************************/
Symbol * lookup(char * s)
{
	Symbol *sp;		/* iterator */
	
	// For each symbol in the linked list:
	for (sp = symlist; sp != (Symbol *) 0; sp = sp->next) {
			
			// If the symbol's name matches s:
			if (strcmp(sp->name, s) == 0) {
				
				// Return the symbol
				return sp;
			}
	}
	
	// List has no matches for s.
	return 0;
}

/**install***************************************
 *  Append a new symbol to the symbol list 		*
 * 	@param s cstring, name of new symbol.  		*
 *  @param t int, type of new symbol.    		*
 *  @param d double, value of new symbol.		*
 *  @return sp pointer to new symbol.			*
 ************************************************/
Symbol * install(char * s,int t, double d)
{
	Symbol * sp;
	char * emalloc();
	
	// Symbol pointer to new memory allocation.
	sp = (Symbol *) emalloc(sizeof(Symbol));
	
	// Symbol name = new allocation of name-length + '\0'.
	sp->name = emalloc(strlen(s)+1);
	
	// Copy parameter s into symbol's name field.
	strcpy(sp->name, s);
	
	// Copy parameter t into symbol's type field.
	sp->type = t;
	
	// Set symbol's union's value field to parameter d.
	sp->u.val = d;
	
	// Append symbol to front of list.
	sp->next = symlist;
	
	// Set head pointer to new symbol.
	symlist = sp;
	
	// Return pointer to new symbol.
	return sp;
}

/**emalloc***************************************
 *  Handles insufficient memory exception. 		*
 * 	@param n Size of requested allocation. 		*
 *  @return p pointer to new block of memory.	*
 ************************************************/
char * emalloc(unsigned n)
{
	char * p;
	
	void * malloc(); //Changed from char * malloc() to silence warning.
	
	// Set p to the result of the malloc call.
	p = malloc(n);
	
	// If p is now null, then the call failed.
	if (p == 0) {
		execerror("Out of memory.", (char *) 0);
	}

	// return a pointer to the new memory block.
	return p;
}

#endif
