#ifndef __hoc_h__
#define __hoc_h__

typedef struct Symbol {
	char * name;
	short type;
	union {
		double val;			// if VAR
		double (*ptr)();	// if BLTIN
	} u;
	struct Symbol * next;
} Symbol;

Symbol * install();

Symbol * lookup();

typedef union Datum { // Interpreter stack type
	double val;
	Symbol *sym;
} Datum;
extern Datum pop();

typedef int (*Inst)(); // machine instruction
#define STOP (Inst) 0

extern Inst prog[];
extern eval(), add(), sub(), mul(), div(), negate(), power();
extern assign(), bltin(), varpush(), constpush(), print();

#endif
