#include "hoc.h"
#include "y.tab.h"

#define NSTACK 256
static Datum stack[NSTACK]; //the stack
static Datum * stackp; 		// Next free spot on stack

#define NPROG 2000
Inst prog[NPROG];	// The machine
Inst *progp;		// Next free spot for code generation
Inst *pc;			// Program counter during execution

// Initialize for code generation
initcode()
{
	stackp = stack;
	progp = prog;
}


push(Datum d) // push d onto stack
{
	if (stackp >= &stack[NSTACK]) {
		execerror("Stack overflow", (char *) 0);
	}
	*stackp++ = d;
}

Datum pop() // pop and return top elem from stack
{
	if (stackp <= stack) {
		execerror("Stack underflow", (char *) 0);
	}
	
	return *--stackp;
}

Inst * code(Inst f) //Install one inst. or operand
{
	Inst *oprogp = progp;
	if (progp >= &prog[NPROG]) {
		execerror("Program too big", (char *) 0);
	}
	*progp++ = f;
	return oprogp;
}

execute(Inst * p) // Run the machine
{
	for (pc = p; *pc != STOP; ) {
		(*(*pc++))();
	}
}

constpush() // push const onto stack
{
	Datum d;
	d.val = ((Symbol *)*pc++)->u.val;
	push(d);
}

varpush() // Push variable onto stack
{
	Datum d;
	d.sym = (Symbol *)(*pc++);
	push(d);
}

add() // Adds two values
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val += d2.val;
	push(d1);
}

sub() // Subtracts d2 from d1
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val -= d2.val;
	push(d1);
}

mul() // Multiplies two values
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val *= d2.val;
	push(d1);
}

div() // Divides d1 by d2
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val /= d2.val;
	push(d1);
}	

power() // Exponentiation: d1->base, d2->exponent (d1^d2)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = pow(d1.val,d2.val);
	push(d1);
}

negate() // Unary minus
{
	Datum d1;
	d1 = pop();
	d1.val *= -1;
	push(d1);
}

eval() // returns the value of a variable
{
	Datum d;
	d = pop();
	if (d.sym->type == UNDEF) {
		execerror("Undefined variable", d.sym->name);
	}
	d.val = d.sym->u.val;
	push(d);
}

assign() // assigns d1 = d2
{
	Datum d1, d2;
	d1 = pop();
	d2 = pop();
	if (d1.sym->type != VAR && d1.sym->type != UNDEF) {
		execerror("Assignment ot non-variable", d1.sym->name);
	}
	d1.sym->u.val = d2.val;
	d1.sym->type = VAR;
	push(d2);
}
print()	// pop top value from stack, print it
{
	Datum d;
	d = pop();
	printf("\t%.8g\n", d.val);
}

bltin()
{
	Datum d;
	d = pop();
	d.val = (*(double (*)())(*pc++))(d.val);
	push(d);
}
	