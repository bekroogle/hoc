/*
 * Yacc File for the HOC (High Order Calculator) described in 
 * Kernighan & Pike's "The Unix Programming Environment. It has been
 * modified and commented for personal use by Benjamin J. Kruger
 * (bekroogle@gmail.com, kruger@nsuok.edu).
 */

%{
#include <stdio.h> // Added to silence warning about printf
#include "hoc.h"
#define code2(c1,c2)	code(c1); code(c2)
#define code3(c1,c2,c3)	code(c1); code(c2); code(c3)
%}
%union {
	Symbol * sym; 	// symbol table pointer
	Inst * inst;	// machine instruction
}
%token <sym> NUMBER VAR BLTIN UNDEF
%type <val> expr asgn
%right '='
%left '+' '-'
%left '*' '/'
%left UNARYMINUS
%right '^'	// exp (2^3 is 2 cubed.)
%%
/* The grammar for the language is below. It has 3 non-terminals:
 * list, asgn, expr. */

list:			// Nothing
			|	list '\n'
			| 	list asgn '\n' 	{ code2(pop, STOP); return 1; }
			|	list expr '\n' 	{ code2(print, STOP); return 1; }
			|	list error '\n' { yyerrok; }
			;

/* asgn
 * The assignment statement sets the $$ value, as well as the the
 * val field of the variable to the value of the expression represented
 * by $3. */
asgn:			VAR '=' expr
				{ code3(varpush, (Inst)$1,assign); }
			
expr:			NUMBER			
				{code2(constpush, (Inst)$1); }
			|	VAR		{ code3(varpush, (Inst)$1, eval); }
			|	asgn
			|	BLTIN '(' expr ')' { code2(bltin, (Inst)$1->u.ptr); }
			| 	'(' expr ')'	
			|	expr '+' expr	{ code(add); }
			|	expr '-' expr	{ code(sub); }
			|	expr '*' expr	{ code(mul); }
			|	expr '/' expr	{ code(div); }
			|	expr '^' expr	{ code(power); }
			|	'-' expr	%prec UNARYMINUS { code(negate); }
			;
%%
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <setjmp.h>
jmp_buf begin;
char *progname;
int lineno = 1;

int main(int argc, char* argv[])
{
	int fpecatch();
	
	progname = argv[0];
	init();
	setjmp(begin);
	signal(SIGFPE, fpecatch);
	
	for (initcode(); yyparse(); initcode()) {
		execute(prog);
	}
	
	return 0;
}
			
execerror(char *s, char *t)
{
	warning(s, t);
	longjmp(begin, 0);
}

fpecatch()
{
	execerror("Floating point exception", (char *) 0);
}

/**yylex*************************************************
 * lexical analyzer.									*
 *******************************************************/
yylex()
{
	int c;
	
	while ((c=getchar()) == ' ' || c == '\t')
		;
	
	if (isalpha(c)) {
		Symbol * s;
		char sbuf[100], *p = sbuf;
		do {
			*p++ = c;
		} while ((c=getchar()) != EOF && isalnum(c));
		ungetc(c, stdin);
		*p = '\0';
		if ((s=lookup(sbuf)) == 0)
			s = install(sbuf, UNDEF, 0.0);
		yylval.sym = s;
		return s->type == UNDEF ? VAR : s->type;
	}
	
	if (c == EOF)
		return 0;
	
	if (c == '.' || isdigit(c)) {
		double d;
		ungetc(c, stdin);
		scanf("%lf", &d);
		yylval.sym = install("", NUMBER, d);
		return NUMBER;
	}
	
	/*
	if (islower(c)) {
		yylval.index = c - 'a';
		return VAR;
	}
	*/
	if (c == '\n')
		lineno++;
	
	return c;
} 

yyerror(char *s)
{
	warning(s, (char *) 0);
}

warning(char* s, char* t)
{
	fprintf(stderr, "%s: %s", progname, s);
	
	if (t)
		fprintf(stderr, " %s", t);
	
	fprintf(stderr, "near line %d\n", lineno);
}
