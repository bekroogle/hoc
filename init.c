#ifndef __init_c__
#define __init_c__

#include "hoc.h"
#include "y.tab.h"
#include <math.h>

extern double Log(), Log10(), Exp(), Sqrt(), integer();

/**constants*************************************
 *    PI: Ratio of circumference to diameter.	*
 *     E: Natural logarithm.					*
 * GAMMA: Euler.								*
 *   DEG: Degrees / Radians.					*
 *   PHI: Golden Ratio.							*
 * 	   0: Zero									*
 ***********************************************/
static struct {
	char * name;
	double eval;
} consts[] = {
	"PI", 3.141592653589793238462643383279502884197169399375105820974944,
	"E", 2.718281828459045235360287471352662497757247093699959574966967,
	"GAMMA", 0.577215664901532860606512090082402431042159335939923598805767,
	"DEG", 57.29577951308232087679815481410517033240547246656432154916024,
	"PHI", 1.618033988749894848204586834365638117720309179805762862135448,
	0, 0
};

/**builtins**************************************
 * Trig idents, logarithms, exp/root, int, abs 	*
 ***********************************************/
static struct { 
	char *name;
	double (*funct)();
} builtins[] = {
	"sin", sin,
	"cos", cos,
	"atan", atan,
	"log", Log, 		//checks argument
	"log10", Log10, 	//checks argument
	"exp", Exp,			//checks argument
	"sqrt", Sqrt,		//checks argument
	"int", integer,
	"abs", fabs,
	0, 0
};

/**init******************************************
 * Installs constants and builtins into table	*
 ***********************************************/
init()
{
	int i;
	Symbol * s;
	
	// For each const in the consts[] array:
	for (i = 0; consts[i].name; i++) {
		install(consts[i].name, VAR, consts[i].eval);
	}
	
	// For each builtin in the builtins[] array:
	for (i = 0; builtins[i].name; i++) {
		s = install(builtins[i].name, BLTIN, 0.0);
		s->u.ptr = builtins[i].funct;
	}
}

#endif
